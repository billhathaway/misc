#!/bin/sh
RESPONSE_FILE="/tmp/responsefile.splunkforwarder.$$"
ADMIN_FILE="/tmp/admin.splunkforwarder.$$"
PASSWORD="badpassword"
INDEXER="192.168.1.64:9997"

# used to set package basedirectory without interaction
BASEDIR=/opt

# we turn off interaction for checking if packaging scripts can be ran
ADMIN_NOASK="
mail=
instance=unique
partial=ask
runlevel=ask
idepend=ask
rdepend=ask
space=ask
setuid=ask
conflict=ask
action=nocheck
networktimeout=60
networkretries=3
authentication=quit
keystore=/var/sadm/security
proxy=
basedir=default
"

handle_error()
{
  echo "$1"
  exit $2
}

usage()
{
  echo "Usage: $0 <path-to-splunk-package>"
  echo "Example: $0 /tmp/splunkforwarder-4.3-115073-solaris-10-intel.pkg.Z"
  exit 1
}

if [ $# -ne 1 ]
then
  usage
fi

PACKAGE=$1

if [ ! -f "${PACKAGE}" ]
then
  handle_error "Could not see package file at ${PACKAGE}" 2
fi

if echo "${PACKAGE}" | egrep '.Z$'
then
  echo "uncompressing package"
  uncompress "${PACKAGE}" || handle_error "Could not uncompress ${PACKAGE}" 3
  PACKAGE=`echo "${PACKAGE}" | sed 's/\.Z$//'`
fi

echo "BASEDIR=${BASEDIR}" > "${RESPONSE_FILE}" || handle_error "Could not create response file ${RESPONSE_FILE}" 4

echo "${ADMIN_NOASK}" > "${ADMIN_FILE}"

pkgadd -r "${RESPONSE_FILE}" -a "${ADMIN_FILE}" -d "${PACKAGE}"  splunkforwarder || handle_error "Could not install package" 5

/opt/splunkforwarder/bin/splunk start --accept-license
/opt/splunkforwarder/bin/splunk edit user admin -password "${PASSWORD}"  -auth admin:changeme
/opt/splunkforwarder/bin/splunk enable boot-start
/opt/splunkforwarder/bin/splunk add forward-server "${INDEXER}"
/opt/splunkforwarder/bin/splunk add monitor /var/adm/messages -sourcetype messages
/opt/splunkforwarder/bin/splunk add monitor /var/adm/sulog   -sourcetype su

rm "${ADMIN_NOASK}" "${RESPONSE_FILE}"
